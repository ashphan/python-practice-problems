# problem 1 -> Yas Function

def yas(num_a):
    if num_a <1 or num_a >30:
        return None
    total_a = ''
    total_a = num_a * 'a'
    return 'Y' + total_a + 's!'
# invoking function

print(yas(5))





# problm 2 -> far away galaxy

def new_hope(num_fars):
    if num_fars < 1 or num_fars > 6:
        return None
    if num_fars == 1:
        final_fars = num_fars * 'far '
    if num_fars <= 6:
        final_fars = (num_fars - 1) * 'far, ' + 'far '
    return 'A long time ago in a galaxy ' + final_fars  + 'away...'

# invoke function

print(new_hope(1))
print(new_hope(3))
print(new_hope(6))
print(new_hope(7))
print(new_hope(0))



# problem 3 -> calculate eldest age

def calculate_eldest_age(youngest_age, middle_age):
    if (
        youngest_age < 0
        or youngest_age > 50
        or youngest_age > middle_age
        or middle_age > 50
    ):
        return None
    return (middle_age - youngest_age) + middle_age

# invoke function

print(calculate_eldest_age(5, 20))
print(calculate_eldest_age(-1,2))
print(calculate_eldest_age(51, 57))
print(calculate_eldest_age(35, 20))
print(calculate_eldest_age(20, 51))



# problem 4 calculate farenheit

def calculate_fahrenheit(celsius):
    if celsius < -40 or celsius > 40:
        return None
    fahrenheit = ((9 / 5) * celsius) + 32
    return int(fahrenheit)

# invoking function
print(calculate_fahrenheit(5))
print(calculate_fahrenheit(-41))
print(calculate_fahrenheit(41))
print(calculate_fahrenheit(10))




# problem 5 - making cupcakes

import math

def profit(num_cupcakes, num_tubes_frosting, tubes_per_cupcake):
    if (
        num_cupcakes < 1
        or num_cupcakes > 100
        or num_tubes_frosting < 10
        or num_tubes_frosting > 50
        or tubes_per_cupcake < 1
        or tubes_per_cupcake > 5
    ):
        return None


    left_over_tubes = num_tubes_frosting % tubes_per_cupcake
    frosted_cupcakes = math.floor(num_tubes_frosting/tubes_per_cupcake)
    cupcakes_no_frosting = num_cupcakes - frosted_cupcakes


    sum = (frosted_cupcakes * 10) + (cupcakes_no_frosting * 4) + (left_over_tubes * 1)
    return sum


print(profit(1,10,1))
print(profit(100,50,5))
print(profit(0,10,1))
print(profit(1,9,1))
print(profit(1,10,0))
print(profit(28,30,4))




# given: frosted cupcakes sells for $10, cupcake with no frosting sells for $4, tub of frosting sells for $1

# constraints:
# 1 <= num_cupcakes <= 100
# 10 <= num_tubes_frosting <= 50
# 1 <= tubes_per_cupcake <= 5

# frosted cupcakes, cupcake with no frosting and left over tubes of frosting there are.


# left over tubes = (num cupcakes - (tubes of frosting us))


# left over tubes  = ((tubes of frosting // tubers per cupcake) - (tubes of frosting % tubes per cupcake))


def profit(num_cupcakes, num_tubes_frosting, tubes_per_cupcake):
    left_over_tubes = ((num_tubes_frosting // tubes_per_cupcake) - (num_tubes_frosting % tubes_per_cupcake))








# COME BACK TO PROB 5
# COME BACK TO PROB 5
# COME BACK TO PROB 5
# COME BACK TO PROB 5
# COME BACK TO PROB 5





# prob 6

def count_words(message):
    word_count = len(message.split())
    if word_count <= 30:
        return True
    else:
        return False
