# create function half_time_scores
# parameters : game length, team 1 scores times, team 2 score times
# return : total number of times both teams scored in the first half


def half_time_scores(
        game_length,
        team_1_score_times,
        team_2_score_times):

    half_game_length = (game_length / 2) * 60
    team_1_total = 0
    team_2_total = 0

    for x in team_1_score_times:
        if x <= half_game_length:
            x = 1
            team_1_total += x
        return team_1_total

    for y in team_2_score_times:
        if y <= half_game_length:
            y = 1
            team_2_total += y
        return team_2_total

    total = team_1_total + team_2_total

    return total


print(half_time_scores(1, [1, 5, 10, 33, 45, 50], [2, 3, 4]))


def half_time_scores(
        game_length,
        team_1_score_times,
        team_2_score_times
):

    team_1_score = 0
    team_2_score = 0

    for x in team_1_score_times:
        if x <= game_length * 60 // 2:
            team_1_score += 1


    for y in team_2_score_times:
        if y <= game_length * 60 // 2:
            team_2_score += 1


    total_score = team_1_score + team_2_score

    return total_score



print(half_time_scores(20,
                       [1100, 1101, 1102, 1103, 1104, 1105, 1106, 1107, 1108, 1109, 1110, 1111, 1112, 1113, 1114, 1115, 1116, 1117, 1118, 1119, 1120, 1121, 1122, 1123],
                       [900, 907, 914, 921, 928, 935, 942, 949, 956, 963, 970, 977, 984, 991, 998] ))



# invoking function inputs
# game_length= 1
# team_1_score_times= [1, 5, 10, 33, 45, 50]
# team_2_score_times= [2, 3, 4]



# game_length= 20
# team_1_score_times= [1100, 1101, 1102, 1103, 1104, 1105, 1106, 1107, 1108, 1109, 1110, 1111, 1112, 1113, 1114, 1115, 1116, 1117, 1118, 1119, 1120, 1121, 1122, 1123]
# team_2_score_times= [900, 907, 914, 921, 928, 935, 942, 949, 956, 963, 970, 977, 984, 991, 998]

# 20
# [1100, 1101, 1102, 1103, 1104, 1105, 1106, 1107, 1108, 1109, 1110, 1111, 1112, 1113, 1114, 1115, 1116, 1117, 1118, 1119, 1120, 1121, 1122, 1123]
# [900, 907, 914, 921, 928, 935, 942, 949, 956, 963, 970, 977, 984, 991, 998]

# game_length= 10
# team_1_score_times= [1, 5, 10, 33, 45, 50, 301]
# team_2_score_times= [2, 3, 4, 400, 502]
