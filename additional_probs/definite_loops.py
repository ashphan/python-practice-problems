

# problem 1 :
def monty(swaps):
    swaps_list = [*swaps]
    print(swaps_list)
    starting_list = [" ", "q", " "]
    for value in swaps_list:
        if 'q' == starting_list[1] and value == 'L':
            starting_list.insert(0, starting_list.pop(1))
        elif 'q' == starting_list[0] and value == 'L':
            starting_list.insert(1, starting_list.pop(0))

        elif 'q' == starting_list[2] and value == 'R':
            starting_list.insert(1, starting_list.pop(2))
        elif 'q' == starting_list[1] and value == 'R':
            starting_list.insert(2, starting_list.pop(1))

        elif 'q' == starting_list[0] and value == 'O':
            starting_list.insert(2, starting_list.pop(0))
        elif 'q' == starting_list[2] and value == 'O':
            starting_list.insert(0, starting_list.pop(2))

    if starting_list == [" ", "q", " "]:
        return 'middle'

    if starting_list == ["q", " ", " "]:
        return 'left'

    if starting_list == [' ', ' ', 'q']:
        return 'right'


# longs solution to prob 1
def monty(swaps):
    queen = "middle"
    for swap in swaps:
        if swap == "L" and queen == "middle":
            queen = "left"
        elif swap == "L" and queen == "left":
            queen = "middle"
        elif swap == "R" and queen == "middle":
            queen = "right"
        elif swap == "R" and queen == "right":
            queen = "middle"
        elif swap == "O" and queen == "left":
            queen = "right"
        elif swap == "O" and queen == "right":
            queen = "left"
    return queen


# //////////////

# problem 2

def num_same_spaces(yesterday,today):
    same_spaces = []
    for x,y in zip(yesterday, today):
        if x == y:
            same_spaces.append(x)
    return len(same_spaces)


# problem 3

def cash_on_hand(expenses):
    monthly = 30
    cash_total = 0
    for value in expenses:
        cash_total = monthly - value
        cash_total += cash_total
    return cash_total

# problem 4

def valid_password(password):
    length = None
    if len(password) >=8 and len(password) <= 12:
        length = True

    lowercase = 0
    for letter in password:
        if letter.islower() == True:
            lowercase += 1

    uppercase = 0
    for letter in password:
        if letter.isupper() == True:
            uppercase += 1

    digits = 0
    for number in password:
        if number.isdigit() == True:
            digits += 1

    if length == True and lowercase >= 2 and uppercase >= 3 and digits >= 2:
        return 'good'
    else:
        return 'bad'


# problem 5

def grade_scantron(submission, answer_key):
    matches = 0
    for a,b in zip(submission, answer_key):
        if a == b:
            matches += 1
    return matches


# problem 6
def language(text):
    if 'ei' not in text and 'ie' not in text:
        return None

    if text.count('ei') > text.count('ie'):
        return 'German'
    if text.count('ie') > text.count('ei'):
        return 'English'
    if text.count('ei') == text.count('ie'):
        return 'Maybe French'
