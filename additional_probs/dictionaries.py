# problem 1


# modelist = {}
# for item in numbers:
#     if item in modeList:
#         modelist[item] += 1
#     else:
#         modelist[item] = 1
# return modelist.values

def mode(numbers):
    mode_list = {}
    for item in numbers:
        if item in mode_list:
            mode_list[item] += 1
        else:
            mode_list[item] = 1

    # modes = []
    # for key in mode_list.keys():
    #     if mode_list[key] == max(mode_list.values()):
    #         modes.append(key)
    # return modes
    # same thing as return below

    return [key for key in mode_list.keys() if mode_list[key] == max(mode_list.values())]




# prob 2
def find_missing_registrant(registrants, finishers):
    finishers_dict = {}
    for participant in finishers:
        if participant in registrants:
            finishers_dict[participant] += 1
        else:
            finishers[participant] += 0
    print(finishers_dict)

    for key,value in finishers_dict.items():
        if value == 0:
            return key
