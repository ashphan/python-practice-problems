def mode(numbers):
    mode_list = {}
    for item in numbers: 
        if item in mode_list:
            mode_list[item] += 1 
        else: 
            mode_list[item] = 1 
    return [key for key in mode_list.keys() if mode_list[key] == max(mode_list.values())]

