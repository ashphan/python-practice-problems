# prob 1 calculate playlist

def calculate_playlist(button_pushes):
    playlist = ["A", "B", "C", "D", "E"]
    for push in button_pushes:
        if push == 1:
            playlist[0], playlist[1] = playlist[1], playlist[0]
        if push == 2:
            playlist.insert(4, playlist.pop(0))
        if push == 3:
            playlist.insert(0, playlist.pop(3))
    return playlist

# prob 2 eating candies

def nia_eat(favorite_color, candy):
    time_fav = 0
    time_not = 0
    for color in candy:
        if color == favorite_color:
            time += 23
        else:
            time_not = 17
    return time_fav + time_not
