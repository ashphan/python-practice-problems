# problem 1 -> friends birthday

# create function - calories
# parameters: entree_num, side_num, dessert_num, drink_num
# return -> total calories for choices

# constraints all inputs are >= 0 and <= 3

# if any value = 0, skipping meal aka 0 calories.

# using dictionary
def calories(entree_num, side_num, dessert_num, drink_num):
    menu = {
        'Entrees' : [0, 522, 399, 501],
        'Sides' : [],
        'Dessert' : [],
        'Drinks' : []

    }
    total_calores = 0
    total_calores += menu['Entrees'][entree_num]
    total_calores += menu['Sides'][side_num]
    total_calores += menu['Dessert'][dessert_num]
    total_calores += menu['Drink'][drink_num]

    return total_calores


# using conditional statements
def calories(entree_num, side_num, dessert_num, drink_num):
    total_calories = 0

    if entree_num == 1:
        total_calories += 522
    elif entree_num == 2:
        total_calories += 399
    elif entree_num == 3:
        total_calories += 501

    if side_num == 1:
        total_calories += 130
    elif side_num == 2:
        total_calories += 125
    elif side_num == 3:
        total_calories += 72

    if dessert_num == 1:
        total_calories += 222
    elif dessert_num == 2:
        total_calories += 391
    elif dessert_num == 3:
        total_calories += 100

    if drink_num == 1:
        total_calories += 10
    elif drink_num == 2:
        total_calories += 8
    elif drink_num == 3:
        total_calories += 120

    return total_calories



# ////
#  problem 2 skating day message


def skating_day_message(month_num, day_num):
    if month_num == 12 and day_num == 4:
        return "YAY! It's World Ice Skating Day!"

    elif month_num == 12 and day_num > 4:
        return "You just missed it. There's another next year!"

    else:
        return "World Ice Skating Day is coming up!"



# ////
#  problem 3 old school sentiment

# create function sentitment
# parameters - message

# constraints = 1 <= len(message) <= 255


# if message.count (happy) > message.count(sad) -> return happy
# if no emoticons in message -> return none
# if mesage.count(sad) > message.count(happy) -> return sad
# if message.count(sad) = message.count(happy) -> return unsure


def sentiment(message):
    if len(message) < 1 or len(message) > 255:
        return False

    if ':-)'not in message and ':-(' not in message:
        return None

    if message.count(':-)') == message.count(':-('):
        return 'unsure'

    if message.count(':-)') > message.count(':-('):
        return 'happy'

    if message.count(':-)') < message.count(':-('):
        return 'sad'


print(sentiment('hello'))
print(sentiment(':-) hi!'))
print(sentiment('bye! :-('))
print(sentiment(':-( :-)'))
print(sentiment(':-:-:(:-)'))



# ////
# problem 4 pizza

# prob 4 --> late night piza
# create pizza satisfaction
# parameters : pizza_size, cheese_multipler
# constraints piza size = size of pizza in inches, cheese mulitpler is 1 2 or 3


# maximally satisfied if pizza is >= 12 inches and has cheese = 3
# really satisfied if pizza >= 10 and has cheese >= 2
# very satisfied otherwise

def pizza_satisfaction (pizza_size, cheese_multiplier):
    if pizza_size >= 12 and cheese_multiplier == 3:
        return 'maximally satisfied'
    if pizza_size >= 10 and cheese_multiplier >= 2:
        return 'really satisfied'
    else:
        return 'very satisfied'



# prob 5 --> shortest route

# create shortest function
# parameters route1 route 2 and route 3
# return shortest route

def shortest(route_1, route_2, route_3):
    if route_1 < route_2 and route_1 < route_3:
        return route_1
    if route_2 < route_1 and route_2 < route_3:
        return route_2
    else:
        return route_3



# prob 6 --> phone scams

# create function scammer
# parameter is number
# return ignore or accept
# conditions
# The first digit is a 2 or 4
#  The fifth digit is a 2 or 4
# The seventh and eight digits are the same
# The last digit is a 0
# -> if all 4 then scammer and return ignore

# otherwise return accept

def scammer(number):
    number_str = str(number)
    if (
        number_str[0] == '2' or number_str[0] == '4'
        and number_str[4] == '2' or number_str[4] == '4'
        and number_str[6] == number_str[7]
        and number_str[9] == 0
    ):
        return 'ignore'
    else:
        return 'accept'


def scammer(number):
    number_str = str(number)
    first_digit = None
    fifth_digit = None
    sev_eight_digit = None
    last_digit = None

    if number_str[0] == '2' or number_str[0] == '4':
        first_digit = True
    if number_str[4] == '2' or number_str[4] == '4':
        fifth_digit = True
    if number_str[6] == number_str[7]:
        sev_eight_digit = True
    if number_str[9] == '0':
        last_digit = True

    if first_digit and fifth_digit and sev_eight_digit and last_digit == True:
        return 'ignore'
    else:
        return 'accept'



# invoking function inputs used:
# number = 2127200090 = ignore
# number = 2986230092 = accept
# number = 2725230180
# number = 2407483300
# number = 2064483342
# number = 2027443440
# number = 4185229000
# number = 4726209990
# number = 4952219937
# number = 4195444470
# number = 4238494436
# number = 4177434510

# come back to prob 6
