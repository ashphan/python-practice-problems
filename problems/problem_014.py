# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three items.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.


def can_make_pasta(ingredients):
    list_needed = ['flour', 'eggs', 'oil']
    for item in ingredients:
        if item in list_needed:
            return True
        else:
            return False

print(can_make_pasta(['flour', 'eggs', 'oil']))


# notes that are different, sole solution does not solve if item is not in the list, if its missing two or more items.


# solutions from solution folder
def can_make_pasta(ingredients):
    if (                                                # solution
        "flour" in ingredients                          # solution
        and "eggs" in ingredients                       # solution
        and "oil" in ingredients                        # solution
    ):                                                  # solution
        return True                                     # solution
    else:                                               # solution
        return False                                    # solution
    # pass                                              # problem
