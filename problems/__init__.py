
def count_greater_than(numbers, lower_limit, upper_limit):
    limit_list = []
    for number in numbers:
        if number > lower_limit and number < upper_limit: 
            limit_list.append(number)
    return len(limit_list)


def third_item(values):
    if len(values) < 3:
        return None 
    return values[2]


def longest(strings):
    count = 0 
    for x in strings: 
        if len(x) > count:
            count = len(x)
            string = x 
    return string 

def gte_10(numbers):
    new_list = []
    for number in numbers:
        if number >= 10:
            new_list.append(number)
    return new_list

def strings_to_integers(strings):
    new_list = []
    for string in strings:
        int_string = int(string)
        new_list.append(int_string)
    return new_list

