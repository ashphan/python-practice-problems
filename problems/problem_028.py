# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.

def remove_duplicate_letters(s):
    if len(s) == 0:
        return s
    no_duplicate = ""
    for letter in s:
        if letter not in no_duplicate:
            # no_duplicate.join(letter) replace with
            no_duplicate += letter
    return no_duplicate

print(remove_duplicate_letters("abcabc"))
